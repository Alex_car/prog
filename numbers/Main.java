package com.prog.numbers;

import java.util.Scanner;

/**
 * Класс для представления целых чисел
 *
 * @author Mashina A.
 */
public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("Введите число: ");
        double numb = scanner.nextDouble();

        if (isInt(numb)) {
            System.out.println("Разряды числа: " + intBits(numb));
        } else {
            System.out.println("Разряды числа: " + doubleBits(numb));
        }
        truth(numb);

    }

    /**
     *
     * @param numb введенное число
     * @return оно целое или неть
     */
    public static boolean isInt(double numb) {
        if (numb % 1 == 0) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param numb введенное число
     * @return целое бинарное число
     */
    public static String intBits(double numb) {
        int newNumb = (int) numb;
        return Integer.toBinaryString(newNumb);
    }

    /**
     *
     * @param numb введенное число
     * @return вещественное бинарное число
     */
    public static String doubleBits(double numb) {
        long longNumb = Double.doubleToLongBits(numb);
        String binDouble = Long.toBinaryString(longNumb);
        return numb > 0 ? "0" + binDouble : binDouble;
    }

    /**
     * Показывает динамику изменения точности делимого числа
     *
     * @param numb введенное число
     */
    public static void truth(double numb) {
        System.out.print("Делитель для ранее введенного числа: ");
        double denominator = scanner.nextDouble();
        System.out.print("Какое число прибавить: ");
        double term = scanner.nextDouble();
        System.out.print("Число до сложения: " + numb / denominator);
        System.out.println();
        System.out.print("Число после сложения: " + ((numb / denominator) + term));
    }
}
