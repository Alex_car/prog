package com.prog.book;

public class Book {
    private String surname;
    private String name;
    private int year;

    public Book(String surname, String name, int year) {
        this.surname = surname;
        this.name = name;
        this.year = year;
    }

    public Book() {
        this("не задано", "не дано", 0);
    }

    public boolean isSameYear(Book secondYear) {
        return this.year == secondYear.getYear();
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Book{" +
                "surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", year=" + year +
                '}';
    }
}
