package com.prog.book;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Book book1 = new Book("autor1", "name1", 2001);
        Book book2 = new Book("autor2", "name2", 1909);
        Book book3 = new Book("autor3", "name3", 2018);
        Book book4 = new Book("autor4", "name4", 1209);
        Book book5 = new Book("autor5", "name5", 2001);
        Book[] books = {book1, book2, book3, book4, book5};
        ArrayList<Book> bookArrayList = new ArrayList<>();

        isFirstNlastSame(bookArrayList);
        System.out.println();
        twentyEighteen(bookArrayList);
        System.out.println();
        System.out.println(addToArrayList(books,bookArrayList));
    }

    private static void twentyEighteen(ArrayList<Book> bookArrayList) {
        int booksSize = bookArrayList.size();
        boolean isBookHere = true;

        System.out.println("Книги 2018 года: ");

        for (int i = 0; i < booksSize; i++) {

            if (bookArrayList.get(i).getYear() == 2018) {
                System.out.println(bookArrayList.get(i) + " ");
                isBookHere = false;
            }
        }
        if (isBookHere) {
            System.out.println("таких нет");
        }
    }

    private static void isFirstNlastSame(ArrayList<Book> bookArrayList) {
        if (bookArrayList.get(0).isSameYear(bookArrayList.get(1))) {
            System.out.println("Первая и вторая книга одного года");
        } else {
            System.out.println("Первая и вторая книга не одного года");
        }
    }

    private static String addToArrayList (Book[] books,ArrayList<Book> bookArrayList){
        int booksSize = books.length;
        for (int i = 0; i <booksSize ; i++) {
            bookArrayList.add(books[i]);
        }
        return bookArrayList.toString();
    }
}
