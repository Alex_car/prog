package com.prog.baggage;

public class Main {
    public static void main(String[] args) {

        Baggage baggage1 = new Baggage ("Багаж 1", 3,2);
        Baggage baggage2 = new Baggage ("Багаж 2", 1,7);
        Baggage baggage3 = new Baggage ("Багаж 3", 2,15);
        Baggage baggage4 = new Baggage ("Багаж 3", 1,1);
        Baggage baggage5 = new Baggage ("Багаж 3", 1,15);
        Baggage[] baggages = {baggage1,baggage2,baggage3,baggage4,baggage5};
        namelaggage(baggages);

    }
    private static void namelaggage (Baggage [] baggages){
        System.out.println("Пассажиры с ручной кладью: ");
        int baggesLenth=baggages.length;

        for (int i = 0; i <baggesLenth ; i++) {
            if (baggages[i].isHandLuggage()){
                System.out.println(baggages[i].getSurname()+" ");
            }
        }
    }
}
