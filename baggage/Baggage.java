package com.prog.baggage;

public class Baggage {
    private String surname;
    private int amountOfBaggage;
    private double totalWeight;


    public Baggage(String surname, int amountOfBaggage, double totalWeight) {
        this.surname = surname;
        this.amountOfBaggage = amountOfBaggage;
        this.totalWeight = totalWeight;
    }

    public Baggage() {
        this("не задано", 0, 0);
    }

    public boolean isHandLuggage() {
        boolean handLuggage = false;
        if (totalWeight < 10 && amountOfBaggage== 1) {
            handLuggage = true;
        }
        return handLuggage;
    }

    public String getSurname() {
        return surname;
    }

}
