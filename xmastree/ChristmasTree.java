package com.prog.xmastree;

@SuppressWarnings("unused")
public class ChristmasTree {
    private double length;
    private Type type;
    private boolean color;
    private double money;

    ChristmasTree(double length, Type type) {
        this.length = length;
        this.type = type;
    }

    public ChristmasTree() {
        this(1, Type.NATURAL);
    }

    ChristmasTree(double length, Type type, Boolean color) {
        this.length = length;
        this.type = type;
        this.color = color;
    }

    ChristmasTree(Type type, double money, Boolean color) {
        this.type = type;
        this.money = money;
        this.color = color;
    }

    ChristmasTree(Type type, double money) {
        this.type = type;
        this.money = money;

    }

    /**
     * Считает стоимость елок в зависимости от типа
     *
     * @return итоговую стоимость
     */
    double price() {
        double price = 0;
        if (type == Type.NATURAL) {
            price = length * 500;
        }

        if (type == Type.ARTIFICIAL) {
            price = length * 3000;
            if (color) {
                price += 500;
            }
        }
        return price;
    }

    /**
     * Считаю какую елку мы можем купить за n сумму
     *
     * @return метраж елки
     */
    double moneyPrice() {
        double treeSize = 0;
        double price = 0;
        for (double length = 0.1; price <= money; length += 0.1) {
            if (type == Type.NATURAL) {
                price = length * 500;
            }

            if (type == Type.ARTIFICIAL) {
                price = length * 3000;
                if (color) {
                    price += 500;
                }
            }
            treeSize = length - 0.1;
        }
        return treeSize;
    }

    @Override
    public String toString() {
        return "ChristmasTree{" +
                "length=" + length +
                ", price=" + price() +
                ", type=" + type +
                '}';
    }

}
