package com.prog.xmastree;

import java.util.Scanner;
/**Класс для реализации действий c елками
 *
 * @author Mashina A.A.
 */
public class Main {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        //Подбираем желаемую елку
        do {
            ChristmasTree mineTree = input();
            System.out.println("Цена желаемой елки: " + mineTree.price() + " р");
            System.out.println();
        } while (treeAgain());

        // Подбираем елку в пределах опеределенной суммы
        do {
            ChristmasTree mineTreeMoney = moneyInput();
            System.out.printf("Вы можете купить елку (в метрах): %.1f", mineTreeMoney.moneyPrice());
            System.out.println();
        } while (treeAgain());
    }

    /**
     * Создает конструктор с количеством денег и типом елки
     * @return конструктор
     */
    private static ChristmasTree moneyInput() {
        System.out.print("Сколько вы можете заплатить? - ");
        double money = scanner.nextDouble();
        System.out.print("Вы хотите натуральную елку? (Да или нет) - ");
        scanner.nextLine();
        String typeStr = scanner.nextLine();
        Type type = Type.NATURAL;
        if (typeStr.equalsIgnoreCase("нет")) {
            type = Type.ARTIFICIAL;
            return new ChristmasTree(type, money, isColor());
        }

        return new ChristmasTree(type, money);
    }

    /**
     * Ввод параментор желаемой елки
     * @return конструктор с елкой
     */
    private static ChristmasTree input() {
        System.out.print("Введите длину в метрах: ");
        double length = scanner.nextDouble();
        System.out.print("Вы хотите натуральную елку? (Да или нет) - ");
        scanner.nextLine();
        String typeStr = scanner.nextLine();

        Type type = Type.NATURAL;
        if (typeStr.equalsIgnoreCase("нет")) {
            type = Type.ARTIFICIAL;
            return new ChristmasTree(length, type, isColor());
        }

        return new ChristmasTree(length, type);
    }

    /**
     * Определяет будет ли искуственная елка другого цвета
     * @return отличается ли цвет от зеленого
     */
    private static boolean isColor() {

        System.out.print("Хотите елку другого цвета, не зеленого? (Да или нет) - ");
        String color = scanner.nextLine();
        boolean isColor = false;
        if (color.equalsIgnoreCase("да")) {
            isColor = true;
        }
        return isColor;
    }

    /**
     * Цикл повторения действий
     * @return повторить цикл или нет
     */
    private static boolean treeAgain() {
        System.out.print("Хотите посмотреть другую елку? - ");
        System.out.println();
        String tree = scanner.nextLine();
        boolean isAgain = false;
        if (tree.equalsIgnoreCase("да")) {
            isAgain = true;
        }
        return isAgain;
    }
}
