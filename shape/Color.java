package com.prog.shape;

public enum Color {
    RED,
    WHITE,
    BLACK,
    BLUE
}
