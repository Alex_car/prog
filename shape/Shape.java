package com.prog.shape;

public abstract class Shape extends Point{
    private Color color;
    private double area;

    public Shape (Color color){
        this.color=color;
    }

    public abstract double area();

    @Override
    public String toString() {
        return "Shape{" +
                "color=" + color +
                ", area=" + area +
                '}';
    }
}
