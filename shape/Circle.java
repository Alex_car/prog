package com.prog.shape;

public class Circle extends Shape{
    private Point center;
    private double radius;

    public Circle (Color color, Point center, double radius){
        super(color);
        this.center=center;
        this.radius=radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "center=" + center +
                ", radius=" + radius +
                '}';
    }

    /**Нахождение площади
     *
     * @return площадь
     */
    @Override
    public double area() {
        return Math.PI*radius*radius;
    }
}
