package com.prog.shape;

public class Square extends Shape {
    private Point corner;
    private double side;

    public Square (Color color, Point corner, double side){
        super(color);
        this.corner=corner;
        this.side=side;
    }

    @Override
    public String toString() {
        return "Square{" +
                "corner=" + corner +
                ", side=" + side +
                '}';
    }

    /**нахождение площади
     *
     * @return площадь
     */
    @Override
    public double area() {
        return side*side;
    }
}
