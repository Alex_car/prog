package com.prog.hospital;

public class Main {

    public static void main(String[] args) {

        Patient p1 = new Patient("Пациент 1", 2001, 18, true);
        Patient p2 = new Patient("Пациент 2", 2000, 11, false);
        Patient p3 = new Patient("Пациент 3", 1999, 1, true);
        Patient p4 = new Patient("Пациент 4", 1985, 8, false);
        Patient p5 = new Patient("Пациент 5", 1997, 195, true);
        Patient[] patients = {p1, p2, p3, p4, p5};

        despanP(patients);


    }

    /**
     * Ищет и выводит массив с пациентами
     * с определенным фильтром
     *
     * @param patients - массив пациентов
     */
    @SuppressWarnings("all")
    private static void despanP(Patient[] patients) {
        System.out.println("Пациенты: ");
        int index = patients.length;
        for (int i = 0; i < index; i++) {
            if (patients[i].isDespan() && patients[i].getBirthYear() < 2000) {
                System.out.println("Пациент{" +
                        "Фамилия='" + patients[i].getSurname() + '\'' +
                        ", Год рождения=" + patients[i].getBirthYear() +
                        ", Номер карты=" + patients[i].getNumOfCard() +
                        ", Деспансаризация пройдена" +
                        '}'
                );
            }
        }
    }
}
