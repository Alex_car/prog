package com.prog.hospital;

@SuppressWarnings("all")
public class Patient {
    private String surname;
    private int birthYear;
    private int numOfCard;
    private boolean despan;

    /**
     * Конструктор объектов
     * с характеристиками пациентов
     *
     * @param surname - Фамилия
     * @param birthYear - Год рождения
     * @param numOfCard - Номер карты
     * @param despan - Прошел ли диспансеризацию
     */
    public Patient(String surname, int birthYear, int numOfCard, boolean despan) {
        this.surname = surname;
        this.birthYear = birthYear;
        this.numOfCard = numOfCard;
        this.despan = despan;
    }

    /**
     * Конструктор объекта
     * со стандартными значениями
     */
    public Patient() {
        this("не указан", 0, 0, false);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "surname='" + surname + '\'' +
                ", birthYear=" + birthYear +
                ", numOfCard=" + numOfCard +
                ", despan=" + despan +
                '}';
    }

    public String getSurname() {
        return surname;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public int getNumOfCard() {
        return numOfCard;
    }

    public boolean isDespan() {
        return despan;
    }
}
