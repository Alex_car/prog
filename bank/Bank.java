package com.prog.bank;

public class Bank {
    private double balance;
    private double percent;

    public Bank(double balance, double percent) {
        this.balance = balance;
        this.percent = percent;
    }

    /**считает начисление процентов за год, но может и не только за год
     *
     * @return состояние счета с процентами
     */
    public double balanceWithPercent() {
        double balanceWithPercent=balance;
        for (int i = 0; i <1 ; i++) {
            balanceWithPercent = (balanceWithPercent * percent/100) + balanceWithPercent;
        }
        return balanceWithPercent;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "balance=" + balance +
                ", percent=" + percent +
                '}';
    }
}
