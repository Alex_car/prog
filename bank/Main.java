package com.prog.bank;
/**Класс для реализации действий c банком
 *
 * @author Mashina A.A.
 */
public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank(156,12);
        System.out.println(bank);
        System.out.printf("Состояние счета с учетом начислений %% за один год: %.2f",bank.balanceWithPercent());
    }
}
