package com.prog.rational;

import java.util.Scanner;
/**Класс для реализации действий с дробями
 *
 * @Autor Mashina A.A.
 */
public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите дробь вида a/b знак a/b");
        String denNDiv = scanner.nextLine();
        Rational[] rationals = input(denNDiv);
        operation(denNDiv, rationals);
    }

    /**ввод выражения с дробями
     *
     * @param denNDiv строка с дробями
     * @return массив элементов из строки с дробями
     */
    private static Rational[] input(String denNDiv) {
        String[] withoutSpace = denNDiv.replaceAll("[\\s]", "/").split("/");

        if (withoutSpace[1].equals(0) || withoutSpace[4].equals(0)) {
            System.out.println("Знаменатель не должен быть равен нулю");
        }

        int a = Integer.parseInt(withoutSpace[0]);
        int b = Integer.parseInt(withoutSpace[1]);
        Rational firstNumb = new Rational(a, b);

        int c = Integer.parseInt(withoutSpace[3]);
        int d = Integer.parseInt(withoutSpace[4]);
        Rational secondNum = new Rational(c, d);

        Rational[] rationals = {firstNumb, secondNum};
        return rationals;

    }

    /**определяет операцию над пробями
     *
     * @param denNDiv строка с дробями
     * @param rationals массив с элементами изстроки с дробями
     */
    private static void operation(String denNDiv, Rational[] rationals) {
        String[] withoutSpace = denNDiv.split("\\s");
        String operation = withoutSpace[1];
        Rational numb;
        switch (operation) {
            case "+":
                numb = rationals[0].plus(rationals[1]);
                System.out.println(numb.getDenominator() + "/" + numb.getDivider());
                break;
            case "-":
                numb = rationals[0].mines(rationals[1]);
                System.out.println(numb.getDenominator() + "/" + numb.getDivider());
                break;
            case "*":
                numb = rationals[0].mult(rationals[1]);
                System.out.println(numb.getDenominator() + "/" + numb.getDivider());
                break;
            case ":":
                numb = rationals[0].div(rationals[1]);
                System.out.println(numb.getDenominator() + "/" + numb.getDivider());
                break;
            default:
                System.out.println("Неправильный знак операций");
                break;
        }
    }
}
