package com.prog.rational;

public class Rational {
    private int denominator;
    private int divider;


    public Rational(int denominator, int divider) {
        this.denominator = denominator;
        this.divider = divider;
    }

    public Rational() {
        this(1, 1);
    }

    @Override
    public String toString() {
        return "Rational{" +
                "denominator=" + denominator +
                ", divider=" + divider +
                '}';
    }

    /**нахождение произведения
     *
     * @param rational дробь
     * @return произведение дробей
     */
    Rational mult(Rational rational) {

        int denominator = this.raw().denominator * rational.raw().denominator;
        int deviver = this.raw().divider * rational.raw().divider;
        return new Rational(denominator, deviver).raw();
    }

    /**нахождение частного
     *
     * @param rational дробь
     * @return частное
     */
    Rational div(Rational rational) {

        int denominator = this.raw().denominator * rational.raw().divider;
        int deviver = rational.raw().denominator * this.raw().divider;
        return new Rational(denominator, deviver).raw();
    }

    /**нахождение суммы
     *
     * @param rational
     * @return сумма дробей
     */
    Rational plus(Rational rational) {
        int denominator = 0;
        int deviver = 0;

        if (this.divider != rational.divider) {
            denominator = this.raw().denominator * rational.raw().divider + rational.raw().denominator * this.raw().divider;
            deviver = rational.raw().divider * this.raw().divider;
        }
        return new Rational(denominator, deviver).raw();
    }

    /**нахождение разности
     *
     * @param rational дробь
     * @return разность
     */
    Rational mines(Rational rational) {
        int denominator = 0;
        int deviver = 0;

        if (this.divider != rational.divider) {
            denominator = this.raw().denominator * rational.raw().divider - rational.raw().denominator * this.raw().divider;
            deviver = rational.raw().divider * this.raw().divider;
        }
        return new Rational(denominator, deviver).raw();
    }

    /**сокращение дробей
     *
     * @return сокращенную дробь
     */
    Rational raw() {
        int a = denominator;
        int b = divider;

       a/= nod(a,b);
       b/= nod(a,b);

        return new Rational(a, b);
    }

    /**нахождение НОД
     *
     * @param a числитель
     * @param b значинатель
     * @return нод
     */
  static int nod (int a, int b){
        a=Math.abs(a);
        b=Math.abs(b);
        while (a!=b){
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }
    public int getDenominator() {
        return denominator;
    }

    public int getDivider() {
        return divider;
    }
}
