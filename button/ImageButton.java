package com.prog.button;

public class ImageButton extends Button {
    private String url;

    public ImageButton(double a, double b, Point point, String url) {
        super(a, b, point);
        this.url = url;
    }

    @Override
    public String toString() {
        return "Button{" +
                "a=" + getA() +
                ", b=" + getB() +
                ", start=" + getStart() +
                ", url='" + url + '\'' +
                '}';
    }
}
