package com.prog.button;

public class Point {
        private double x;
        private double y;

        public Point (double x, double y){
            this.x=x;
            this.y=x;
        }
        public Point (){
            this(0,0);
        }

        @Override
        public String toString() {
            return "Point{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
}
