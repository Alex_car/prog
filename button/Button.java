package com.prog.button;

public class Button {
    private double a;
    private double b;
    private Point start;

    public Button(double a, double b, Point point) {
        this.a = a;
        this.b = b;
        this.start = point;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public Point getStart() {
        return start;
    }
}
