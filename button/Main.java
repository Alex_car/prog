package com.prog.button;

public class Main {
    public static void main(String[] args) {
        TextButton textButton = new TextButton(10,10, new Point(2,1),"text");
        ImageButton imageButton = new ImageButton(5,3,new Point(9,7),"https://sun9-11.userapi.com/c200720/v200720479/e117/DGCCclRlVuU.jpg");

        System.out.println(textButton);
        System.out.println(imageButton);
    }
}
