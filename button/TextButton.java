package com.prog.button;

public class TextButton extends Button {
    private String name;

    public TextButton(double a, double b, Point point, String name) {
        super(a, b, point);
        this.name = name;
    }

    @Override
    public String toString() {
        return "Button{" +
                "a=" + getA() +
                ", b=" + getB() +
                ", start=" + getStart() +
                ", name='" + name + '\'' +
                '}';
    }
}
