package com.prog.warship;

import java.util.ArrayList;

public class Ship {
    private ArrayList<Integer> location = new ArrayList<>();


    Ship(int firstCoord, int shipLength) {
        for (int i = firstCoord; i < firstCoord + shipLength; i++) {
            location.add(i);
        }
    }

    /**определяет попадание и потопленность
     *
     * @param shot ячейка выстрела
     * @return состояние корабля
     */
    String shot(int shot) {
        String message = "Мимо";
        if (location.contains(shot)) {
            location.remove(location.indexOf(shot));
            if (location.isEmpty()) {
                message = "Потоплен";
            }
            message = "Попал";
        }
        return message;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "ship=" + location +
                '}';
    }
}
