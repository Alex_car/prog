package com.prog.warship;

import java.util.Scanner;
/**Класс для реализации действий с кораблем
 *
 * @Autor Mashina A.A.
 */
public class Main {
    public static void main(String[] args) {
        int length = 1 + (int) (Math.random() * 4);
        int gateway = (length+1) + (int) (Math.random() * (20-length));
        int randomFirstCoord = (int) (Math.random() * (gateway-length));
        System.out.println("Длина корабля: " + length);
        System.out.println("Длина шлюза: " + gateway);


        Ship ship = new Ship(randomFirstCoord, length);
        System.out.println(ship);

        startGame(gateway, ship);
    }

    /**реализация выстрелов по кораблю
     *
     * @param gateway шлюз
     * @param ship длина корабля
     */
    private static void startGame(int gateway, Ship ship) {
        Scanner scanner = new Scanner(System.in);
        int shot;
        int attempt=0;
        String result;
        do {
            System.out.println("В какую ячейку среляем: (0-" + (gateway - 1) + ")");
            shot = scanner.nextInt();
            result = ship.shot(shot);
            System.out.println(result);
            attempt++;
        } while (!result.equals("Потоплен"));
        System.out.println("Вы потопили корабль c "+attempt+" попытки");
    }
}
