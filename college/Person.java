package com.prog.college;

public class Person {
    private String surname;
    private Gender gender;

    public Person(String surname, Gender gender) {
        this.surname = surname;
        this.gender = gender;
    }

    public String getSurname() {
        return surname;
    }

    public Gender getGender() {
        return gender;
    }
}
