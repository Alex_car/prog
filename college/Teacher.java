package com.prog.college;

public class Teacher extends Person {
    private String discipline;
    private boolean isCurator;

    public Teacher(String surname, Gender gender, String discipline, boolean isCurator) {
        super(surname, gender);
        this.discipline = discipline;
        this.isCurator = isCurator;
    }

    @Override
    public String toString() {
        return "Преподаватели{" +
                "Фамилия= " + getSurname() +
                ", Пол= " + getGender() +
                ", Дисциплина='" + discipline + '\'' +
                ", Куратор=" + isCurator +
                '}';
    }

    /**явл. ли преподаватель куратором
     *
     * @return да или нет
     */
    public boolean isCurator() {
        return isCurator;
    }
}
