package com.prog.college;

public class Student extends Person {
    private int entrance;
    private String codeOfProfession;

    public Student(String surname, Gender gender,int entrance, String codeOfProfession) {
        super(surname, gender);
        this.entrance = entrance;
        this.codeOfProfession = codeOfProfession;
    }

    @Override
    public String toString() {
        return "Студент{" +
                "Фамилия= " + getSurname() +
                ", Пол= " + getGender() +
                ", Год поступления=" + entrance +
                ", Код специальности='" + codeOfProfession + '\'' +
                '}';
    }

    public int getEntrance() {
        return entrance;
    }

}
