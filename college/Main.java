package com.prog.college;

import java.util.Scanner;
/**Класс для реализации действий с преподавателями и студентами
 *
 * @Autor Mashina A.A.
 */
public class Main {
    public static void main(String[] args) {
        Student student1 = new Student("Иванова", Gender.FEMALE, 2017, "14.54.25");
        Student student2 = new Student("Смирнов", Gender.MALE, 2007, "14.54.25");
        Student student3 = new Student("Машина", Gender.FEMALE, 2017, "14.54.25");
        Student student4 = new Student("Пинчук", Gender.MALE, 2009, "14.54.25");
        Student student5 = new Student("Тропанова", Gender.FEMALE, 2017, "14.54.25");
        Student student6 = inputStudent();
        Student[] students = {student1, student2, student3, student4, student5, student6};

        System.out.println();

        Teacher teacher1 = new Teacher("Дорофеев", Gender.MALE, "физика", true);
        Teacher teacher2 = new Teacher("Соломинский", Gender.MALE, "физ-ра", true);
        Teacher teacher3 = new Teacher("Стадник", Gender.FEMALE, "английский", false);
        Teacher teacher4 = new Teacher("Пузренков", Gender.MALE, "WS", true);
        Teacher teacher5 = new Teacher("Воробьева", Gender.FEMALE, "матиматика", false);
        Teacher teacher6 = inputTeacher();
        Teacher[] teachers = {teacher1, teacher2, teacher3, teacher4, teacher5, teacher6};

        Person[][] people = {teachers, students};

        System.out.println();
        System.out.print("Количество девушек 2017 года поступления: " + qualityOfGirls(students));

        System.out.println();
        System.out.println("Преподаватели-кураторы: ");
        curators(teachers);
        System.out.println();
        System.out.println("Преподаватели и студеты мужского пола:");
        teachersNStudentsMale(people);
    }

    /**ввод преподавателя с клавиатуры
     *
     * @return объект с преподавателем
     */
    private static Teacher inputTeacher() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Фамилия преподавателя: ");
        String surname = scanner.nextLine();
        System.out.print("Пол(м или ж): ");
        String gender = scanner.nextLine();
        Gender gender1 = Gender.FEMALE;
        if (gender.equalsIgnoreCase("м")) {
            gender1 = Gender.MALE;
        }
        if (gender.equalsIgnoreCase("ж")) {
            gender1 = Gender.FEMALE;
        }
        System.out.print("Дисциплина: ");
        String discipline = scanner.nextLine();
        //  scanner.nextLine();
        System.out.print("Куратор(да или нет): ");
        String isCurator = scanner.nextLine();
        boolean isCurator1 = false;
        if (isCurator.equalsIgnoreCase("да")) {
            isCurator1 = true;
        }
        if (isCurator.equalsIgnoreCase("нет")) {
            isCurator1 = false;
        }
        return new Teacher(surname, gender1, discipline, isCurator1);
    }

    /**ввод студента с клавиатуры
     *
     * @return объект со студентом
     */
    public static Student inputStudent() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Фамилия студента: ");
        String surname = scanner.nextLine();
        System.out.print("Пол(м или ж): ");
        String gender = scanner.nextLine();
        System.out.print("Год поступления: ");
        int entrance = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Код специальности: ");
        String codeOfProfession = scanner.nextLine();
        Gender gender1 = Gender.FEMALE;
        if (gender.equalsIgnoreCase("м")) {
            gender1 = Gender.MALE;
        }
        if (gender.equalsIgnoreCase("ж")) {
            gender1 = Gender.FEMALE;
        }
        return new Student(surname, gender1, entrance, codeOfProfession);
    }

    /**нахождение количества студенток с опр. годом поступления
     *
     * @param students массив студентов
     * @return количество студенток
     */
    public static int qualityOfGirls(Student[] students) {
        int quality = 0;
        for (int i = 0; i < students.length; i++) {
            if (students[i].getEntrance() == 2017) {
                quality++;
            }
        }
        return quality;
    }

    /**вывод преподавателей кураторов
     *
     * @param teachers массив кураторов
     */
    public static void curators(Teacher[] teachers) {
        for (int i = 0; i < teachers.length; i++) {
            if (teachers[i].isCurator()) {
                System.out.println(teachers[i] + " ");
            }
        }
    }

    /**нахождение и вывод мужчин преподавательй и студентов
     *
     * @param people массив преподавателей и студентов
     */
    public static void teachersNStudentsMale(Person[][] people) {
        for (int i = 0; i < people.length; i++) {
            for (int j = 0; j < people[0].length; j++) {
                if (people[i][j].getGender() == Gender.MALE) {
                    System.out.println(people[i][j] + " ");
                }
            }
        }
    }
}
