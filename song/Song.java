package com.prog.song;

public class Song {
    private String name;
    private String autor;
    private int timeSec;

    public Song(String name, String autor, int timeSec) {
        this.name = name;
        this.autor = autor;
        this.timeSec = timeSec;
    }

    public Song() {
        this("Неизвестно", "Неизвестен", 0);
    }

    /**
     * возвращает категорию песен по продолжительности
     *
     * @return одну из категорий
     */
    public String category() {
        String categoryLong = "error";
        if (timeSec <= 120) {
            categoryLong = "short";
        }
        if (timeSec > 120 && timeSec <= 240) {
            categoryLong = "medium";
        }
        if (timeSec > 240) {
            categoryLong = "long";
        }
        return categoryLong;
    }

    /**
     * сравнивает текущую категорию с поступающей
     *
     * @param song однин объект из массива
     * @return одинаковые ли категории у песен
     */
    public boolean isSameCategory(Song song) {
        boolean isSameCategory = false;
        if (song.category().equals(this.category())) {
            isSameCategory = true;
        }
        return isSameCategory;
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", autor='" + autor + '\'' +
                ", timeSec=" + timeSec +
                '}';
    }

    public int getTimeSec() {
        return timeSec;
    }
}
