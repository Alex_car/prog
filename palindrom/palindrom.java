package com.prog.palindrom;

import java.util.Scanner;
/**Класс для реализации действий робота
 *
 * @Autor Mashina A.A.
 */
public class palindrom {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите строку: ");
        String line = scanner.nextLine();

        if (isPalendrom(line)) {
            System.out.println("Строка палиндром");
        } else {
            System.out.println("Строка не палиндром");
        }
    }

    /**является ли строка палиндромом
     *
     * @param line строка с предложением
     * @return палиндром или нет
     */
    private static boolean isPalendrom(String line) {
        String workLineOne = line.replaceAll("[\\s()?:!.,;{}\"-]", "");
        String workLineTwo = new StringBuffer(workLineOne).reverse().toString();
        boolean retVal = workLineOne.equalsIgnoreCase(workLineTwo);
        return retVal;
    }
}



